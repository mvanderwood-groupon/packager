require 'zip'

Zip.setup do |config| 
    config.on_exists_proc = true
end

class Packager
  def initialize(input_dir, output_dir, options={})
    @input_dir  = input_dir
    @output_dir = output_dir
    compile_skipped_dirs options[:skip]
  end

  def write
    entries = Dir.entries(@input_dir) - [".", ".."]

    ::Zip::File.open @output_dir, ::Zip::File::CREATE do |io|
      write_entries entries, "", io
    end
  end

private 

  def compile_skipped_dirs(skipped_dirs)
    decompressed_dirs = skipped_dirs.map { |d| d.split "*" }
    @skipped_names, @skipped_exts = [], []
    decompressed_dirs.each do |dir|
      @skipped_names << dir[0] if dir
      @skipped_exts << dir[-1]
    end
  end

  def write_entries(entries, path, io)
    entries.each do |entry|
      zip_file_path = path == "" ? entry : File.join(path, entry)
      disk_file_path = File.join @input_dir, zip_file_path
      puts "Deflating #{disk_file_path}"

      if need_to_write? disk_file_path
        if File.directory? disk_file_path
          recursively_deflate_directory disk_file_path, io, zip_file_path
        else
          put_into_archive disk_file_path, io, zip_file_path
        end
      end
    end
  end

  def recursively_deflate_directory(disk_file_path, io, zip_file_path)
    io.mkdir zip_file_path
    subdir = Dir.entries(disk_file_path) - [".", ".."]
    write_entries subdir, zip_file_path, io
  rescue Errno::ENOENT => e
    log e.message
  end

  def put_into_archive(disk_file_path, io, zip_file_path)
    io.get_output_stream zip_file_path do |f|
      f.write(File.open(disk_file_path, "rb").read)
    end
  end

  def need_to_write?(file_path)
    !skipped?(file_path) && update_pending?(file_path)
  end

  def skipped?(file_path)
    name  = file_path.split(".")[0]
    ext   = file_path.split(".")[-1]
    @skipped_names.include?(name) || @skipped_exts.include?(ext)
  end

  def update_pending?(file_path)
    true
  end

  def log(message)
    visual_aid = "====================================================\n"
    puts visual_aid*2 + message + "\n" + visual_aid*2
  end
end

packager = Packager.new ARGV[0], ARGV[1], skip: [".DS_Store", "*.pem"]
packager.write